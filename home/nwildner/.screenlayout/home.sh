#!/bin/sh
xrandr --output eDP-1 --mode 1920x1200 --pos 0x1120 --rotate normal --output DP-1 --off --output HDMI-1 --off --output DP-2 --off --output DP-3 --off --output DP-4 --off --output DP-2-8 --mode 2560x1440 --pos 4480x0 --rotate left --output DP-2-1 --primary --mode 2560x1440 --pos 1920x1120 --rotate normal
